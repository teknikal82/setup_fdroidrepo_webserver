## Setting up a web server for your F-Droid compatible repo
This setup was developed and tested with the smallest Debian server [offered by Hetzner](https://www.hetzner.com/cloud),
a CX11 for less than EUR 3 per month. You can create and manage it via their „cloud console“.
If you want to access the machine via SSH, simply upload your public SSH key via their console.
**I strongly recommend leaving all accounts „password-less“** (i.e. permitting SSH access only
with SSH keys). Attempts to log in via SSH started only minutes after the VM was created; permitting
no password-based login mitigates risks.

## Pre-conditions
* you have **a server with a fresh Debian system** available (should do with any, not just with Hetzner's).
  This setup was tested with Debian 11 (bullseye) on a Hetzner CX11, but basically should run on
  any fresh and recent Debian install (even on Debian 10 aka buster).
* a **public DNS record** points to the hostname you configure to use. Without that, creation
  of certificates for HTTPS access will fail.
* you have **`root` access** to the machine; to do its job, the script needs to be run as `root`.  
  (if you don't trust the script (congrats! Unless you know me and trust me, you shouldn't
  trust the script without proper checks), it provides a `DRY_RUN` mode where it installs into
  a location of your choice, see the config parameters `DRY_RUN` and `INSTALL_ROOT`; commands
  which would change the system and/or require `root` would then just be `echo`d. Dry run
  must be performed with a user other than `root`, „for reasons“ :)

## Setup TL;DR
To configure your shiny new default Debian VM for use as F-Droid repo, simply copy the
`config.sample` file to `config` and adjust it to reflect your needs, then run `setup_server`.
For details what it does, see below.

## Installing packages
As of this writing, the Hetzner VM runs on Debian 11 (bullseye). Only `main contrib non-free`
are enabled, and only for the default package sources, which is fully sufficient. Recommends is
disabled by default (good!) – so if wanted, we need to explicitly pass `--install-recommends`.
The script will do so for selected packages.

First step is running `apt update && apt upgrade` (to initialize the repository index and pull in
latest (security) updates). Then the script installs the following packages:

* optional, if configured (`INSTALL_MC=1`): `apt install mc --install-recommends`
  (recommends: perl, unzip, mime-stuff) – Midnight Commander for easy navigation on the shell.  
  If installed, it will later have its „directory hotlist“ pre-configured for most essential
  locations (Apache config, Apache logs, web server DOCUMENT_ROOT)
* `apache2` recommends `ssl-cert` (for installing certs), cannot hurt, especially as we plan using
  „apache mode“ for maintaining our certificates with `acme.sh` later: `apt install --install-recommends apache2`
* `socat` is only needed for `acme.sh` if we want to use stand-alone mode (as we use „apache“ mode instead,
  it's commented out and won't be installed currently)
* `apt install fail2ban --install-recommends`: [Fail2Ban](https://en.wikipedia.org/wiki/Fail2ban) to keep
  the bad guys (and gals) out of our system (or at least make it much harder for them to get in). The
  recommends are needed for our setup.
* we will need `acme.sh` to take care for SSL certs. This requires the hostname being available via public
  DNS **before running this script** (see pre-conditions above).

## Apache configuration adjustment
* see `files/etc/apache2/sites-available` for site configuration
* see `files/var/www/fdroid` for the initial `DOCUMENT_ROOT`
* see `files/root` for what will be adjusted with `root`s home directory. Additionally, you can place
  files into `files/root/.ssh` (e.g. `authorized_keys`) to have them properly placed.
* `a2enmod headers`: the `headers` module so we can provide a more secure setup (XSS protection, CSP and more)
* `a2dissite 000-default`: kick out the "dummy site"
* `a2ensite ${DOMAIN}`: activate our new site as "plain http"

## SSL Certificates
We will use [acme.sh](https://github.com/acmesh-official/acme.sh) to manage our SSL certificates.
Detailed instructions can be found in the project's README at the mentioned URL. Installation
is done with `curl https://get.acme.sh | sh -s email=my@example.com` (replacing the email
address accordingly; this is the account email, i.e. the administrative address of the person
responsible for the certificates – set by `ADMIN_MAIL` in the `config` file).

Steps to issue certs (only needed once) are in the script, as well as steps to install them
to our Apache config. `acme.sh` will take care for renewal automatically (check with
`crontab -l`).

Following this, `mod_ssl` will be enabled for Apache, along with the `https` config for our site,
and Apache will be restarted to pick up those changes.

## Fine-Tuning
We will redirect all HTTP requests to HTTPS. To do so, we add this line to
`/etc/apache2/sites-available/${DOMAIN}.conf`:

    Redirect / https://${DOMAIN}/

We will use `fail2ban` to protect against attacks, and (next to SSH) configure Apache stuff with it.
See `files/etc/fail2ban` for our adjustments to its defaults.

## Final words
The resulting system is nice and slim. Checking with `top`, with all the above steps performed and
the processes running (incl. Apache and [Fail2Ban](https://en.wikipedia.org/wiki/Fail2ban), just
a little more than 80 megabytes of RAM and about 1.5 gigabyte disk space were being used. Our
configuration earned us A+ ratings with both, [SSL Labs](https://ssllabs.com/) (only with `OLD_ANDROIDS=0`,
else it's capped to B) and [Mozilla Observatory](https://observatory.mozilla.org/).

At least with the Hetzner setup, a firewall will probably not needed as there are no open ports to
block (except maybe 68):

```
# netstat -tulpen
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address  Foreign Address  State    User  Inode  PID/Program name
tcp        0      0 0.0.0.0:22     0.0.0.0:*        LISTEN   0     13216  665/sshd: /usr/sbin
tcp6       0      0 :::22          :::*             LISTEN   0     13227  665/sshd: /usr/sbin
tcp6       0      0 :::443         :::*             LISTEN   0     66553  19493/apache2
tcp6       0      0 :::80          :::*             LISTEN   0     66549  19493/apache2
udp        0      0 0.0.0.0:68     0.0.0.0:*                 0     12576  461/dhclient
```

In case you wonder about the directory structure in the `DOCUMENT_ROOT`, this was decided upon
with a reason:

* `/var/www/fdroid` was chosen as `DOCUMENT_ROOT` to permit you to easily setup additional
  „VHosts“ with their `DOCUMENT_ROOT` inside `/var/www` later.
* the URL path of `/fdroid/repo` (resulting in `/var/www/fdroid/fdroid/repo`) was needed
  so the F-Droid client claims the intent when opening the URL scanning the QR code, to make
  it very easy for users to add the repo: scan the code, tap „open“, choose the F-Droid client
  to complete the action with, tap „add“ – done.

## What's left?
Well, adjust the `serverwebroot` in your fdroidserver's `config.yml` to point to your new web server.
This should be the directory containing the `repo` and `archive` directories, i.e.

```yaml
serverwebroot: fdroid@<hostname>:/var/www/fdroid/fdroid`
```

(of course replacing `<hostname>` with the proper host name). If you want an unprivileged user to
do nothing but push to the repo via `rsync`, you can set up the entry in
`/home/fdroid/.ssh/authorized_keys` on the web server to look like

```
command="rsync --server -logDtpre.iLsfx --log-format=X --delete --delay-updates . /var/www/fdroid/fdroid",no-port-forwarding,no-X11-forwarding,no-agent-forwarding,no-pty <SSH key> fdroid push rsync
```

Replace `<SSH key>` by the key you want to grant access to. If you sufficiently trust the
keyholder (and maybe want to use the same key for interactive access with the `fdroid` user),
simply omit everything before the `<SSH key>`. For details on this, also see:
[Running a Primary Mirror (receiving syncs via push)](https://f-droid.org/en/docs/Running_a_Mirror/#running-a-primary-mirror-receiving-syncs-via-push#running-a-primary-mirror-receiving-syncs-via-push)

Now deploy using `fdroid deploy` :)


## Credits
Thanks a lot to [MetatransApps](https://metatransapps.com/) who sponsored this setup. With
Krasimir (who ordered the CX11 so we could test the steps on it) I evaluated the needs and
went through the steps, ending up with a nice A+ rated server hosting an F-Droid repo (theirs).
The data in the `config.sample` reflect Metatrans and their new repo; if you're interested in
chess and educational games, pay them a visit!

## License
This project uses the [GNU Affero General Public License](LICENSE) version 3:

Automated setup of an Apache based web server on Debian for use with an F-Droid repository  
Copyright © 2021 Andreas Itzchak Rehberg

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
